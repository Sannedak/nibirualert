package com.ltst.nibirualert.data.model.preview

import com.google.gson.annotations.SerializedName

data class AsteroidsPreviewRequest(

    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("end_date")
    val endDate: String
)