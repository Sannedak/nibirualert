package com.ltst.nibirualert.data.repository.preview

import com.ltst.nibirualert.data.model.preview.AsteroidsPreviewResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AsteroidListRetrofit {

    @GET("/neo/rest/v1/feed")
    suspend fun getAsteroidsList(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String
    ): Response<AsteroidsPreviewResponse>
}