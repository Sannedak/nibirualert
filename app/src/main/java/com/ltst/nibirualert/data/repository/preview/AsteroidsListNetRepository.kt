package com.ltst.nibirualert.data.repository.preview

import com.ltst.nibirualert.data.model.preview.AsteroidsPreviewResponse
import java.util.*

interface AsteroidsListNetRepository {

    suspend fun getAsteroidsList(startDate: Date, endDate: Date): AsteroidsPreviewResponse
}