package com.ltst.nibirualert.data.model.preview

import com.google.gson.annotations.SerializedName

data class AsteroidsPreviewResponse(

    @SerializedName("near_earth_objects")
    val days: Map<String, List<AsteroidPreviewResponse>>
)

data class AsteroidPreviewResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("is_potentially_hazardous_asteroid")
    val isPotentiallyHazardous: Boolean,
    @SerializedName("estimated_diameter")
    val estimatedDiameter: DiameterResponse,
    @SerializedName("close_approach_data")
    val approachData: ArrayList<ApproachDataResponse>
) {
    fun calculateAverageDiameter(): Double {
        return (estimatedDiameter.meters.max - estimatedDiameter.meters.min) / 2
    }
}

data class DiameterResponse(
    @SerializedName("kilometers")
    val kilometers: DiameterMinMaxResponse,
    @SerializedName("meters")
    val meters: DiameterMinMaxResponse
)

data class DiameterMinMaxResponse(
    @SerializedName("estimated_diameter_min")
    val min: Double,
    @SerializedName("estimated_diameter_max")
    val max: Double
)

data class ApproachDataResponse(
    @SerializedName("close_approach_date")
    val arrivalDate: String,
    @SerializedName("relative_velocity")
    val relativeVelocity: RelativeVelocityResponse,
    @SerializedName("miss_distance")
    val missDistance: MissDistanceResponse,
    @SerializedName("orbiting_body")
    val orbitingBody: String
)

data class RelativeVelocityResponse(
    @SerializedName("kilometers_per_second")
    val kilometersPerSecond: Double,
    @SerializedName("kilometers_per_hour")
    val kilometersPerHour: Double
)

data class MissDistanceResponse(
    @SerializedName("astronomical")
    val astronomical: Double,
    @SerializedName("lunar")
    val lunar: Double,
    @SerializedName("kilometers")
    val kilometers: Double,
    @SerializedName("miles")
    val miles: Double
)