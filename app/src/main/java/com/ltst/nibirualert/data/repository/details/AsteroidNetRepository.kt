package com.ltst.nibirualert.data.repository.details

import com.ltst.nibirualert.data.model.details.AsteroidResponse

interface AsteroidNetRepository {

    suspend fun getAsteroid(asteroidId: String): AsteroidResponse
}