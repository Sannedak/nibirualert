package com.ltst.nibirualert.data.repository.details

import com.ltst.nibirualert.data.model.details.AsteroidResponse
import retrofit2.Retrofit

class AsteroidNasaNetRepository(retrofit: Retrofit) : AsteroidNetRepository {

    private val service by lazy { retrofit.create(AsteroidRetrofit::class.java) }

    override suspend fun getAsteroid(asteroidId: String): AsteroidResponse {
        val response = service.getAsteroidDetail(asteroidId)
        return response.body()!!
    }
}