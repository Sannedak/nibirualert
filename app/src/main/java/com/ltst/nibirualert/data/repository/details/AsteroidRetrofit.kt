package com.ltst.nibirualert.data.repository.details

import com.ltst.nibirualert.data.model.details.AsteroidResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface AsteroidRetrofit {

    @GET("/neo/rest/v1/neo/{asteroid_id}")
    suspend fun getAsteroidDetail(
        @Path("asteroid_id") asteroidId: String
    ): Response<AsteroidResponse>
}