package com.ltst.nibirualert.data.network

import okhttp3.Interceptor
import okhttp3.Response

class ApiKeyInterceptor : Interceptor {

    companion object {
        private const val API_KEY = "weHS3uJIBPchOxVLIJ46W2ZQ9S2BMsyoLi5fIqpz"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val url = request.url().newBuilder().addQueryParameter("api_key",
            API_KEY
        ).build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}