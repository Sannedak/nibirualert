package com.ltst.nibirualert.data.repository.preview

import com.ltst.nibirualert.data.model.preview.AsteroidsPreviewResponse
import com.ltst.nibirualert.data.model.preview.AsteroidsPreviewRequest
import retrofit2.Retrofit
import java.text.SimpleDateFormat
import java.util.*

class AsteroidsListNasaNetRepository(private val retrofit: Retrofit) :
    AsteroidsListNetRepository {

    companion object {
        private const val NET_DATE_PATTERN = "yyyy-MM-dd"
    }

    private val toNetDateFormatter =
        SimpleDateFormat(NET_DATE_PATTERN, Locale.ENGLISH)

    override suspend fun getAsteroidsList(
        startDate: Date,
        endDate: Date
    ): AsteroidsPreviewResponse {
        val service = retrofit.create(AsteroidListRetrofit::class.java)
        val request = createRequest(startDate, endDate)
        val response = service.getAsteroidsList(request.startDate, request.endDate)
        return response.body()!!
    }

    private fun createRequest(startDate: Date, endDate: Date) =
        AsteroidsPreviewRequest(
            toNetDateFormatter.format(startDate),
            toNetDateFormatter.format(endDate)
        )
}