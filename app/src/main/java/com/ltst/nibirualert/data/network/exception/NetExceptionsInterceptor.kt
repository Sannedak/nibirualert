package com.ltst.nibirualert.data.network.exception

import com.ltst.nibirualert.data.network.connection.NetConnectionStatusChecker
import okhttp3.Interceptor
import okhttp3.Response

class NetExceptionsInterceptor(private val netChecker: NetConnectionStatusChecker)
    : Interceptor {

    private val restExceptionFactory = ServerExceptionFactory()

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!netChecker.isActive()) throw NoInternetException()
        val response = chain.proceed(chain.request())
        if (response.code() >= 400) throw restExceptionFactory.createFromResponse(response)
        return response
    }
}
