package com.ltst.nibirualert.data.network

import android.app.Application
import android.util.Log
import com.ltst.nibirualert.BuildConfig
import com.ltst.nibirualert.data.network.connection.NetConnectionStatusCheckerImpl
import com.ltst.nibirualert.data.network.exception.NetExceptionsInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class RetrofitFactory {

    companion object {
        private const val SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS = 15L
        private const val DISK_CACHE_SIZE_IN_BYTES = 50 * 1024 * 1024L
        private const val BASE_URL = "https://api.nasa.gov"
        private const val CACHE_CHILD_PATH_NAME = "http"

        private const val LOGGING_TAG = "OKHTTP"
    }

    fun create(
        app: Application
    ): Retrofit {
        val okHttpClient = createOkHttpClient(app)
        val converterFactory = GsonConverterFactory.create()
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(converterFactory)
            .build()
    }

    private fun createOkHttpClient(app: Application): OkHttpClient {
        val file = File(app.cacheDir, CACHE_CHILD_PATH_NAME)
        val cache = okhttp3.Cache(file, DISK_CACHE_SIZE_IN_BYTES)
        val interceptors = createInterceptors(app)
        return OkHttpClient.Builder()
            .connectTimeout(SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS, TimeUnit.SECONDS)
            .readTimeout(SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS, TimeUnit.SECONDS)
            .cache(cache).apply {
                interceptors.forEach { addInterceptor(it) }
            }
            .build()
    }

    private fun createInterceptors(app: Application): List<Interceptor>{
        val netChecker = NetConnectionStatusCheckerImpl(app)
        return listOf(
            createLoggingInterceptor(),
            NetExceptionsInterceptor(netChecker),
            ApiKeyInterceptor()
        )
    }

    private fun createLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor {
            if (BuildConfig.DEBUG) {
                Log.i(LOGGING_TAG, it)
            }
        }.setLevel(HttpLoggingInterceptor.Level.BODY)
    }
}
