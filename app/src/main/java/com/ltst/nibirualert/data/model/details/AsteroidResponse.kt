package com.ltst.nibirualert.data.model.details

import com.google.gson.annotations.SerializedName
import com.ltst.nibirualert.data.model.preview.ApproachDataResponse
import com.ltst.nibirualert.data.model.preview.DiameterResponse

class AsteroidResponse(
    @SerializedName("is_potentially_hazardous_asteroid")
    val isPotentiallyHazardous: Boolean,
    @SerializedName("estimated_diameter")
    val estimatedDiameter: DiameterResponse,
    @SerializedName("orbital_data")
    val orbitData: OrbitResponse?,
    @SerializedName("close_approach_data")
    val ApproachData: List<ApproachDataResponse>
)

data class OrbitResponse(
    @SerializedName("orbit_class")
    val orbitClass: OrbitClassResponse
)

data class OrbitClassResponse(
    @SerializedName("orbit_class_type")
    val classType: String,
    @SerializedName("orbit_class_description")
    val classDescription: String
)
