package com.ltst.nibirualert.data.network.connection

interface NetConnectionStatusChecker {

    fun isActive(): Boolean
}
