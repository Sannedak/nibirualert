package com.ltst.nibirualert.di

import android.app.Application
import com.ltst.nibirualert.data.network.RetrofitFactory
import com.ltst.nibirualert.data.repository.details.AsteroidNasaNetRepository
import com.ltst.nibirualert.data.repository.details.AsteroidNetRepository
import com.ltst.nibirualert.data.repository.preview.AsteroidsListNasaNetRepository
import com.ltst.nibirualert.data.repository.preview.AsteroidsListNetRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class NetworkModule {

    private val factory = RetrofitFactory()

    @Provides
    @ProjectScope
    fun provideRetrofit(app: Application): Retrofit = factory.create(app)


    @Provides
    @ProjectScope
    fun provideAsteroidListRepository(retrofit: Retrofit): AsteroidsListNetRepository =
        AsteroidsListNasaNetRepository(retrofit)

    @Provides
    @ProjectScope
    fun provideAsteroidRepository(retrofit: Retrofit): AsteroidNetRepository =
        AsteroidNasaNetRepository(retrofit)
}