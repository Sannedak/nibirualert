package com.ltst.nibirualert.di

import com.ltst.nibirualert.presentation.fragments.details.AsteroidPresenter
import com.ltst.nibirualert.presentation.fragments.preview.AsteroidsPreviewPresenter
import dagger.Component

@ProjectScope
@Component(modules = [NetworkModule::class, ProjectModule::class, MapperModule::class])
interface ProjectComponent {

    fun inject(presenter: AsteroidsPreviewPresenter)

    fun inject(presenter: AsteroidPresenter)
}