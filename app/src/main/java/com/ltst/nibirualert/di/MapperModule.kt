package com.ltst.nibirualert.di

import android.app.Application
import com.ltst.nibirualert.presentation.fragments.details.model.UiAsteroidMapper
import com.ltst.nibirualert.presentation.fragments.preview.model.UiAsteroidPreviewMapper
import dagger.Module
import dagger.Provides

@Module
class MapperModule {

    @Provides
    @ProjectScope
    fun provideAsteroidPreviewMapper(app: Application) = UiAsteroidPreviewMapper(app.resources)

    @Provides
    @ProjectScope
    fun provideAsteroidMapper(app: Application) = UiAsteroidMapper(app.resources)
}