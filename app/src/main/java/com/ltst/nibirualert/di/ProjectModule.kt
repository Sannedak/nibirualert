package com.ltst.nibirualert.di

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class ProjectModule(val app: Application) {

    @Provides
    @ProjectScope
    fun getApplicationContext(): Application = app
}