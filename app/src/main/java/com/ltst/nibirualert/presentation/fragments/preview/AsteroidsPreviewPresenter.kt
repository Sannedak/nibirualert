package com.ltst.nibirualert.presentation.fragments.preview

import com.ltst.nibirualert.data.repository.preview.AsteroidsListNetRepository
import com.ltst.nibirualert.presentation.MainActivity
import com.ltst.nibirualert.presentation.base.exception.createErrorHandler
import com.ltst.nibirualert.presentation.fragments.details.AsteroidScreenParams
import com.ltst.nibirualert.presentation.fragments.preview.model.UiAsteroidPreviewMapper
import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@InjectViewState
class AsteroidsPreviewPresenter : MvpPresenter<AsteroidsPreviewView>() {

    companion object {
        private const val UI_DATE_PATTERN = "dd.MM.yy"
    }

    @Inject
    lateinit var repository: AsteroidsListNetRepository
    @Inject
    lateinit var mapper: UiAsteroidPreviewMapper

    private lateinit var startDate: Date
    private lateinit var endDate: Date
    private val calendar by lazy { Calendar.getInstance() }
    private val dateFormatter by lazy { SimpleDateFormat(UI_DATE_PATTERN, Locale.ENGLISH) }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadAsteroidsWithInitialDate()
    }

    fun onRefresh() {
        val handler = createErrorHandler {
            viewState.showErrorDialog(it)
            viewState.setRefreshing(false)
        }
        loadAsteroids(handler)
    }

    private fun loadAsteroidsWithInitialDate() {
        startDate = calendar.time
        calendar.add(Calendar.DAY_OF_MONTH, 7)
        endDate = calendar.time
        setDateButtons()
        loadAsteroidsWithProgress()
    }

    fun onAsteroidClick(asteroid: AsteroidScreenParams) {
        viewState.showAsteroidScreen(asteroid)
    }

    fun onStartDateClick() = viewState.showStartDatePickerDialog()

    fun onEndDateClick() = viewState.showEndDatePickerDialog()

    fun onDateSet(startDate: Date, endDate: Date) {
        calendar.time = startDate
        if (this.startDate != startDate) {
            calendar.add(Calendar.DAY_OF_MONTH, 7)
            this.endDate = calendar.time
        } else {
            this.endDate = endDate
        }
        this.startDate = startDate
        setDateButtons()
        loadAsteroidsWithProgress()
    }

    private fun setDateButtons() {
        val startDateText = dateFormatter.format(startDate)
        val endDateText = dateFormatter.format(endDate)
        viewState.setDateOnButtons(startDate, endDate)
        viewState.changeTextOnDateButtons(startDateText, endDateText)
    }

    private fun loadAsteroidsWithProgress() {
        val handler = createErrorHandler {
            viewState.showError(it, ::loadAsteroidsWithProgress)
            viewState.setRefreshing(false)
        }
        loadAsteroids(handler)
    }

    private fun loadAsteroids(handler: CoroutineExceptionHandler) {
        viewState.setRefreshing(true)
        CoroutineScope(Dispatchers.Main).launch(handler) {
            val response = withContext(Dispatchers.IO) {
                val asteroidResponseMap = repository.getAsteroidsList(startDate, endDate).days
                asteroidResponseMap.flatMap { (_, list) -> list }
            }
            val asteroidPreviewList = mapper.map(response)
            viewState.showContent(asteroidPreviewList)
            viewState.setRefreshing(false)
        }
    }
}