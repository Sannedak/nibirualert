package com.ltst.nibirualert.presentation.fragments.preview

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.ltst.nibirualert.R
import com.ltst.nibirualert.presentation.fragments.preview.model.UiAsteroidPreview
import com.ltst.nibirualert.di.DaggerProjectComponent
import com.ltst.nibirualert.di.ProjectModule
import com.ltst.nibirualert.presentation.MainActivity
import com.ltst.nibirualert.presentation.base.exception.ExceptionInfo
import com.ltst.nibirualert.presentation.fragments.details.AsteroidScreenParams
import com.ltst.nibirualert.presentation.setVisible
import kotlinx.android.synthetic.main.fragment_asteroids_preview.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import java.text.SimpleDateFormat
import java.util.*

class AsteroidsPreviewFragment : MvpAppCompatFragment(), AsteroidsPreviewView {

    companion object {
        fun newInstance() = AsteroidsPreviewFragment()
        private const val UI_DATE_PATTERN = "dd.MM.yy"
    }

    @InjectPresenter
    lateinit var presenter: AsteroidsPreviewPresenter

    private val asteroidsPreviewAdapter by lazy{ AsteroidsPreviewAdapter(presenter::onAsteroidClick) }
    private val startCalendarInstance by lazy { Calendar.getInstance() }
    private val endCalendarInstance by lazy { Calendar.getInstance() }
    private val startDatePickerDialog by lazy {
        val startDateSetListener =
            createDateSetListener(date_start_button, startCalendarInstance)
        createDatePickerDialog(startDateSetListener, startCalendarInstance)!!
    }
    private val endDatePickerDialog by lazy {
        val endDateSetListener =
            createDateSetListener(date_end_button, endCalendarInstance)
        createDatePickerDialog(endDateSetListener, endCalendarInstance)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_asteroids_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEventListeners()
        initializeRecycler()
    }

    private fun injectDependencies() {
        DaggerProjectComponent.builder().projectModule(ProjectModule(activity!!.application))
            .build()
            .inject(presenter)
    }

    private fun initEventListeners() {
        date_start_button.setOnClickListener { presenter.onStartDateClick() }
        date_end_button.setOnClickListener { presenter.onEndDateClick() }
        refresh_list.setOnRefreshListener { presenter.onRefresh() }
    }

    private fun initializeRecycler() {
        asteroid_list.adapter = asteroidsPreviewAdapter
    }

    override fun showErrorDialog(info: ExceptionInfo) {
        val builder = AlertDialog.Builder(context!!)
        builder
            .setMessage(getString(info.messageRes))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.ok_button)
            ) { dialog, _ -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    override fun showAsteroidScreen(asteroid: AsteroidScreenParams) {
        val mainActivity = activity as MainActivity
        mainActivity.putAsteroidOnScreen(asteroid)
    }

    override fun changeTextOnDateButtons(startDate: String, endDate: String) {
        date_start_button.text = startDate
        date_end_button.text = endDate
    }

    override fun setDateOnButtons(startDate: Date, endDate: Date) {
        endCalendarInstance.time = startDate
        endDatePickerDialog.datePicker.minDate = startCalendarInstance.timeInMillis
        endCalendarInstance.add(Calendar.DAY_OF_YEAR, 7)
        endDatePickerDialog.datePicker.maxDate = endCalendarInstance.timeInMillis
        startCalendarInstance.time = startDate
        endCalendarInstance.time = endDate
    }

    override fun showStartDatePickerDialog() {
        startDatePickerDialog.show()
    }

    override fun showEndDatePickerDialog() {
        endDatePickerDialog.show()
    }

    override fun showError(info: ExceptionInfo, retryCallback: () -> Unit) {
        error_retry_button.setOnClickListener { retryCallback() }
        error_text.text = getString(info.messageRes)
        changeScreenState(ScreenState.ERROR)
    }

    override fun showContent(list: List<UiAsteroidPreview>) {
        asteroidsPreviewAdapter.swapItems(list)
        asteroid_list.adapter?.notifyDataSetChanged()
        setCountLabel(list.size)
        changeScreenState(ScreenState.CONTENT)
    }

    override fun setRefreshing(isRefreshing: Boolean){
        refresh_list.isRefreshing = isRefreshing
    }

    private fun setCountLabel(itemCount: Int?) {
        find_in_period.text =
            resources.getQuantityString(R.plurals.things_plural, itemCount!!, itemCount)
    }

    private fun createDateSetListener(targetButton: Button, calendar: Calendar) =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val sdf = SimpleDateFormat(UI_DATE_PATTERN, Locale.US)
            targetButton.text = sdf.format(calendar.time)
            presenter.onDateSet(startCalendarInstance.time, endCalendarInstance.time)
        }

    private fun createDatePickerDialog(
        dateSetListener: DatePickerDialog.OnDateSetListener,
        dateCalendar: Calendar
    ) = context?.let {
        DatePickerDialog(
            it,
            dateSetListener,
            dateCalendar.get(Calendar.YEAR),
            dateCalendar.get(Calendar.MONTH),
            dateCalendar.get(Calendar.DAY_OF_MONTH)
        )
    }

    private fun changeScreenState(state: ScreenState) {
        asteroid_list.setVisible(state == ScreenState.CONTENT)
        error_container.setVisible(state == ScreenState.ERROR)
    }

    private enum class ScreenState {
        CONTENT,
        ERROR
    }
}
