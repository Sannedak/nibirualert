package com.ltst.nibirualert.presentation.fragments.details

import java.io.Serializable

data class AsteroidScreenParams(
    val id: Int,
    val name: String,
    val imageSize: Int
): Serializable