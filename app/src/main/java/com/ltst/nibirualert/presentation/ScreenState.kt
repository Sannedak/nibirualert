package com.ltst.nibirualert.presentation

enum class ScreenState {
    LOADING,
    ERROR,
    CONTENT
}