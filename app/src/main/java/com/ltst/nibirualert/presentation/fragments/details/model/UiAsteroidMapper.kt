package com.ltst.nibirualert.presentation.fragments.details.model

import android.content.res.Resources
import com.ltst.nibirualert.R
import com.ltst.nibirualert.data.model.details.AsteroidResponse
import com.ltst.nibirualert.data.model.preview.DiameterResponse
import kotlin.math.roundToInt

class UiAsteroidMapper(private val resources: Resources) {

    private val mapVisits by lazy { UiAsteroidVisitsMapper(resources) }

    fun map(response: AsteroidResponse) = with(response) {
        UiAsteroid(
            isPotentiallyHazardous,
            mapDiameter(estimatedDiameter),
            orbitData?.orbitClass?.classType,
            orbitData?.orbitClass?.classDescription,
            mapVisits.map(ApproachData)
        )
    }

    private fun mapDiameter(diameter: DiameterResponse): String {
        val isSmallDiameter = diameter.meters.min < 1000
        return if (isSmallDiameter) mapDiameterMeter(diameter) else mapDiameterKilometer(diameter)
    }

    private fun mapDiameterMeter(diameter: DiameterResponse): String {
        val postfix = resources.getString(R.string.meter)
        return resources.getString(
            R.string.diameter,
            diameter.meters.min.roundToInt(),
            diameter.meters.max.roundToInt(),
            postfix
        )
    }

    private fun mapDiameterKilometer(diameter: DiameterResponse): String {
        val postfix = resources.getString(R.string.kilometer)
        return resources.getString(
            R.string.diameter,
            diameter.kilometers.min.roundToInt(),
            diameter.kilometers.max.roundToInt(),
            postfix
        )
    }
}