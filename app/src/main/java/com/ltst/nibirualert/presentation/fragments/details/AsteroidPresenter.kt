package com.ltst.nibirualert.presentation.fragments.details

import com.ltst.nibirualert.R
import com.ltst.nibirualert.data.repository.details.AsteroidNetRepository
import com.ltst.nibirualert.presentation.base.exception.createErrorHandler
import com.ltst.nibirualert.presentation.fragments.details.model.UiAsteroidMapper
import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class AsteroidPresenter(private val id: Int) : MvpPresenter<AsteroidView>() {

    @Inject
    lateinit var repository: AsteroidNetRepository
    @Inject
    lateinit var mapper: UiAsteroidMapper

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadAsteroidsWithProgress()
    }

    fun onRefresh() {
        val handler = createErrorHandler {
            viewState.showErrorDialog(it)
            viewState.setRefreshing(false)
        }
        loadAsteroidInformation(handler)
    }

    private fun loadAsteroidsWithProgress() {
        viewState.setRefreshing(true)
        val handler = createErrorHandler {
            viewState.showError(it, ::loadAsteroidsWithProgress)
        }
        loadAsteroidInformation(handler)
    }
    
    fun onShareClick(asteroidName: String) {
        viewState.share(R.string.share_text, asteroidName)
    }

    private fun loadAsteroidInformation(handler: CoroutineExceptionHandler) {
        CoroutineScope(Dispatchers.Main).launch(handler) {
            val response = withContext(Dispatchers.IO) { repository.getAsteroid(id.toString()) }
            val asteroidInformation = mapper.map(response)
            viewState.showContent(asteroidInformation)
            viewState.setRefreshing(false)
        }
    }
}