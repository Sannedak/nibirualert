package com.ltst.nibirualert.presentation.fragments.preview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ltst.nibirualert.R
import com.ltst.nibirualert.presentation.fragments.details.AsteroidScreenParams
import com.ltst.nibirualert.presentation.fragments.preview.model.UiAsteroidPreview
import com.ltst.nibirualert.presentation.setVisible
import kotlinx.android.synthetic.main.list_asteroid_item.view.*

class AsteroidsPreviewAdapter(private val showAsteroidScreen: (asteroid: AsteroidScreenParams) -> Unit) :
    RecyclerView.Adapter<AsteroidHolder>() {

    private val asteroids = ArrayList<UiAsteroidPreview>()

    override fun getItemCount() = asteroids.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AsteroidHolder {
        return AsteroidHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_asteroid_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AsteroidHolder, position: Int) {
        holder.bind(asteroids[position], showAsteroidScreen)
    }

    fun swapItems(response: List<UiAsteroidPreview>) {
        asteroids.clear()
        asteroids.addAll(response)
    }
}

class AsteroidHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(
        asteroid: UiAsteroidPreview,
        showAsteroidScreen: (asteroidTransactionInfo: AsteroidScreenParams) -> Unit
    ) {
        val asteroidInformation =
            AsteroidScreenParams(asteroid.id, asteroid.name, asteroid.imageSizePx)
        itemView.setOnClickListener { showAsteroidScreen(asteroidInformation) }
        itemView.asteroid_name.text = asteroid.name
        itemView.asteroid_hazard.setVisible(asteroid.isPotentiallyHazardous)
        itemView.asteroid_diameter.text = asteroid.diameter
        itemView.asteroid_approach_date.text = asteroid.approachDate
        itemView.asteroid_relative_velocity.text = asteroid.relativeVelocity
        itemView.asteroid_miss_distance.text = asteroid.missDistance
        itemView.asteroid_image.layoutParams.height = asteroid.imageSizePx
        itemView.asteroid_image.layoutParams.width = asteroid.imageSizePx
    }
}
