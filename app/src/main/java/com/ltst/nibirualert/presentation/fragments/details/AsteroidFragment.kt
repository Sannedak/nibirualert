package com.ltst.nibirualert.presentation.fragments.details

import android.app.AlertDialog
import android.content.Intent
import android.graphics.LightingColorFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ltst.nibirualert.R
import com.ltst.nibirualert.di.DaggerProjectComponent
import com.ltst.nibirualert.di.ProjectModule
import com.ltst.nibirualert.presentation.base.exception.ExceptionInfo
import com.ltst.nibirualert.presentation.fragments.details.model.UiAsteroid
import com.ltst.nibirualert.presentation.setVisible
import kotlinx.android.synthetic.main.fragment_asteroid_detail.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class AsteroidFragment : MvpAppCompatFragment(), AsteroidView {

    companion object {
        private const val ASTEROID = "asteroid"

        fun newInstance(asteroid: AsteroidScreenParams): Fragment = AsteroidFragment().apply {
            arguments = Bundle().apply { putSerializable("asteroid", asteroid) }
        }
    }

    @InjectPresenter
    lateinit var presenter: AsteroidPresenter

    private val asteroidAdapter by lazy { AsteroidAdapter() }
    private val screenParams by lazy { getArgumentsOfScreen() }

    @ProvidePresenter
    fun providePresenter(): AsteroidPresenter {
        return AsteroidPresenter(screenParams.id)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_asteroid_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEventListners()
        changeToolbarProperties()
        initializeRecycler()
    }

    private fun injectDependencies() {
        DaggerProjectComponent.builder().projectModule(ProjectModule(activity!!.application))
            .build().inject(presenter)
    }

    private fun initEventListners() {
        refresh_detail.setOnRefreshListener { presenter.onRefresh() }
    }

    private fun getArgumentsOfScreen() =
        arguments?.getSerializable(ASTEROID) as AsteroidScreenParams

    private fun changeToolbarProperties() {
        asteroid_toolbar.navigationIcon?.colorFilter = LightingColorFilter(0x000000, 0xffffff)
        asteroid_toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        asteroid_name.text = screenParams.name
        share_button.setOnClickListener { presenter.onShareClick(screenParams.name) }

    }

    private fun initializeRecycler() {
        history_visit_list.adapter = asteroidAdapter
    }

    override fun showErrorDialog(info: ExceptionInfo) {
        val builder = AlertDialog.Builder(context!!)
        builder
            .setMessage(getString(info.messageRes))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.ok_button)
            ) { dialog, _ -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    override fun showError(info: ExceptionInfo, retryCallback: () -> Unit) {
        error_retry_button.setOnClickListener { retryCallback() }
        error_text.text = getString(info.messageRes)
        changeScreenState(ScreenState.ERROR)
    }

    override fun showContent(asteroid: UiAsteroid) {
        asteroidAdapter.swapItems(asteroid.visits)
        history_visit_list.adapter?.notifyDataSetChanged()
        asteroid_image.layoutParams.height = screenParams.imageSize
        asteroid_image.layoutParams.width = screenParams.imageSize
        detail_asteroid_hazard.setVisible(asteroid.isPotentiallyHazardous)
        detail_asteroid_diameter.text = asteroid.diameter
        detail_asteroid_class.text =
            resources.getString(R.string.orbit_class_type, asteroid.orbitClassType)
        detail_asteroid_class_description.text = asteroid.orbitClassDescription
        changeScreenState(ScreenState.CONTENT)
    }

    override fun setRefreshing(isRefreshing: Boolean) {
        refresh_detail.isRefreshing = isRefreshing
    }
    
    override fun share(shareBodyResource: Int, asteroidName: String) {
        val body = resources.getString(shareBodyResource, asteroidName)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, body)
        startActivity(
            Intent.createChooser(
                intent,
                resources.getString(R.string.share_via)
            )
        )
    }

    private fun changeScreenState(state: ScreenState) {
        history_visit_list.setVisible(state == ScreenState.CONTENT)
        detail_orbit.setVisible(state == ScreenState.CONTENT)
        asteroid_image.setVisible(state == ScreenState.CONTENT)
        history_visit.setVisible(state == ScreenState.CONTENT)
        error_container.setVisible(state == ScreenState.ERROR)
    }

    private enum class ScreenState {
        CONTENT,
        ERROR
    }
}