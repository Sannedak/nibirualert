package com.ltst.nibirualert.presentation.fragments.details.model

data class UiAsteroid (
    val isPotentiallyHazardous: Boolean,
    val diameter: String,
    val orbitClassType: String?,
    val orbitClassDescription: String?,
    val visits: List<VisitsDetail>
)

data class VisitsDetail(
    val approachDate: String,
    val relativeVelocity: String,
    val missDistance: String,
    val orbitPlanet: String
)