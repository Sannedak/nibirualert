package com.ltst.nibirualert.presentation.base.exception

import kotlinx.coroutines.CoroutineExceptionHandler

/***
 * This class is a wrapper around CoroutineExceptionHandler.
 * It provides an easy way to pass an onError callback which is to be executed
 * when an exception in coroutine is caught.
 * Also onError receives an already mapped message which describes an error.
 *
 * Use this class to handle exceptions in a coroutine like this:
 *
 * val handler = createErrorHandler { onError(it) }
 * launch(handler) {
 *     // code
 * }
 *
 */
class CommonExceptionHandler(
    private val onError: ((ExceptionInfo) -> Unit)? = null
) {

    private val exceptionInfoMapper by lazy { ExceptionInfoMapper() }

    val coroutineHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError?.let {
            val exceptionInfo = exceptionInfoMapper.map(throwable)
            it.invoke(exceptionInfo)
        }
    }
}

fun createErrorHandler(onError: (ExceptionInfo) -> Unit) =
    CommonExceptionHandler(onError).coroutineHandler
