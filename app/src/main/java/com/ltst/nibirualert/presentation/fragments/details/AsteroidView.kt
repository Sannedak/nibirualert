package com.ltst.nibirualert.presentation.fragments.details

import com.ltst.nibirualert.presentation.ChangeViewStateStrategy
import com.ltst.nibirualert.presentation.base.exception.ExceptionInfo
import com.ltst.nibirualert.presentation.fragments.details.model.UiAsteroid
import moxy.MvpView
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(ChangeViewStateStrategy::class)
interface AsteroidView : MvpView {

    fun showContent(asteroid: UiAsteroid)

    fun showError(info: ExceptionInfo, retryCallback: () -> Unit)

    @StateStrategyType(SkipStrategy::class)
    fun showErrorDialog(info: ExceptionInfo)

    @StateStrategyType(SkipStrategy::class)
    fun setRefreshing(isRefreshing: Boolean)
    
    @StateStrategyType(SkipStrategy::class)
    fun share(shareBodyResource: Int, asteroidName: String)
}