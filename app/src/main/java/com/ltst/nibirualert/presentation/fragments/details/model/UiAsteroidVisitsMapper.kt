package com.ltst.nibirualert.presentation.fragments.details.model

import android.content.res.Resources
import com.ltst.nibirualert.R
import com.ltst.nibirualert.data.model.preview.ApproachDataResponse
import com.ltst.nibirualert.data.model.preview.MissDistanceResponse
import com.ltst.nibirualert.data.model.preview.RelativeVelocityResponse
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class UiAsteroidVisitsMapper(private val resources: Resources) {

    companion object {
        private val NET_DATE_PATTERN = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        private val UI_DATE_PATTERN = SimpleDateFormat("dd.MM.yyyy\nhh:mm", Locale.ENGLISH)
    }

    private val dateFormatterNet by lazy { NET_DATE_PATTERN }
    private val dateFormatterUi by lazy { UI_DATE_PATTERN }

    fun map(history: List<ApproachDataResponse>): List<VisitsDetail> = history.map { mapVisits(it) }

    private fun mapVisits(response: ApproachDataResponse): VisitsDetail = with(response) {
        VisitsDetail(
            mapApproachDate(arrivalDate),
            mapRelativeVelocity(relativeVelocity),
            mapMissDistance(missDistance),
            mapOrbitingBody(orbitingBody)
        )
    }

    private fun mapApproachDate(arrivalDate: String): String {
        val netArrivalDate = dateFormatterNet.parse(arrivalDate)
        return dateFormatterUi.format(netArrivalDate!!)
    }

    private fun mapRelativeVelocity(velocity: RelativeVelocityResponse): String {
        val isSmallVelocity = velocity.kilometersPerHour < 1000
        return if (isSmallVelocity) mapVelocityKmPerHour(velocity) else mapVelocityKmPerSec(velocity)
    }


    private fun mapVelocityKmPerHour(velocity: RelativeVelocityResponse): String {
        val postfix = resources.getString(R.string.km_per_hour)
        return resources.getString(
            R.string.visit_velocity,
            velocity.kilometersPerHour.roundToInt(),
            postfix
        )
    }

    private fun mapVelocityKmPerSec(velocity: RelativeVelocityResponse): String {
        val postfix = resources.getString(R.string.km_per_second)
        return resources.getString(
            R.string.visit_velocity,
            velocity.kilometersPerSecond.roundToInt(),
            postfix
        )
    }

    private fun mapMissDistance(missDistance: MissDistanceResponse): String = when {
        missDistance.miles < 1000 -> mapMissDistanceMiles(missDistance)
        missDistance.kilometers < 1000 -> mapMissDistanceKilometers(missDistance)
        missDistance.lunar < 1000 -> mapMissDistanceLunar(missDistance)
        else -> mapMissDistanceLunarAstronomical(missDistance)
    }

    private fun mapMissDistanceMiles(missDistance: MissDistanceResponse): String {
        val milesPlural = resources.getQuantityString(
            R.plurals.mile_plural,
            missDistance.miles.roundToInt()
        )
        return resources.getString(
            R.string.visit_miss_on,
            missDistance.miles,
            milesPlural
        )
    }

    private fun mapMissDistanceKilometers(missDistance: MissDistanceResponse): String {
        val prefix = resources.getString(R.string.kilometer)
        return resources.getString(
            R.string.miss_on,
            missDistance.kilometers,
            prefix
        )
    }

    private fun mapMissDistanceLunar(missDistance: MissDistanceResponse): String {
        val lunarPlural = resources.getQuantityString(
            R.plurals.lunar_plural,
            missDistance.lunar.roundToInt()
        )
        return resources.getString(
            R.string.visit_miss_on,
            missDistance.lunar.roundToInt(),
            lunarPlural
        )
    }

    private fun mapMissDistanceLunarAstronomical(missDistance: MissDistanceResponse): String {
        val prefix = resources.getString(R.string.astronomic)
        return resources.getString(
            R.string.visit_miss_on,
            missDistance.astronomical,
            prefix
        )
    }

    private fun mapOrbitingBody(orbitingBody: String): String =
        resources.getString(R.string.visit_orbit_around, orbitingBody)
}