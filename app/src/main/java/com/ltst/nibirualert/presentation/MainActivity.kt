package com.ltst.nibirualert.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.ltst.nibirualert.R
import com.ltst.nibirualert.presentation.fragments.details.AsteroidScreenParams
import com.ltst.nibirualert.presentation.fragments.details.AsteroidFragment
import com.ltst.nibirualert.presentation.fragments.preview.AsteroidsPreviewFragment

class MainActivity : AppCompatActivity() {

    companion object {
        private const val LIST_FRAGMENT_TAG = "list"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) putFirstFragmentOnScreen(AsteroidsPreviewFragment.newInstance())
    }

    private fun putFirstFragmentOnScreen(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment, LIST_FRAGMENT_TAG)
            .commitNow()
    }

    fun putAsteroidOnScreen(asteroid: AsteroidScreenParams) {
        val asteroidFragmentInstance = AsteroidFragment.newInstance(asteroid)
        val transaction = supportFragmentManager.beginTransaction()
        val asteroidFragment = supportFragmentManager.findFragmentByTag(LIST_FRAGMENT_TAG)
        transaction.addToBackStack(LIST_FRAGMENT_TAG)
        transaction.hide(asteroidFragment!!)
        transaction.add(R.id.fragment_container, asteroidFragmentInstance)
        transaction.commit()
    }
}
