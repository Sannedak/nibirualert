package com.ltst.nibirualert.presentation.fragments.preview.model

data class UiAsteroidPreview(
    val id: Int,
    val name: String,
    val diameter: String,
    val isPotentiallyHazardous: Boolean,
    val approachDate: String,
    val relativeVelocity: String,
    val missDistance: String,
    val imageSizePx: Int
)
