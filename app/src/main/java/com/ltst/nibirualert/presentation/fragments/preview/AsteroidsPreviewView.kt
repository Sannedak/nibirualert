package com.ltst.nibirualert.presentation.fragments.preview

import com.ltst.nibirualert.presentation.ChangeViewStateStrategy
import com.ltst.nibirualert.presentation.base.exception.ExceptionInfo
import com.ltst.nibirualert.presentation.fragments.details.AsteroidScreenParams
import com.ltst.nibirualert.presentation.fragments.preview.model.UiAsteroidPreview
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType
import java.util.*

@StateStrategyType(ChangeViewStateStrategy::class)
interface AsteroidsPreviewView : MvpView {

    fun showContent(list: List<UiAsteroidPreview>)

    fun showError(info: ExceptionInfo, retryCallback: () -> Unit)

    @StateStrategyType(SkipStrategy::class)
    fun showErrorDialog(info: ExceptionInfo)

    @StateStrategyType(SkipStrategy::class)
    fun setRefreshing(isRefreshing: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showAsteroidScreen(asteroid: AsteroidScreenParams)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setDateOnButtons(startDate: Date, endDate: Date)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun changeTextOnDateButtons(startDate: String, endDate: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showStartDatePickerDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showEndDatePickerDialog()
}