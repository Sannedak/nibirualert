package com.ltst.nibirualert.presentation.base.exception

import androidx.annotation.StringRes

class ExceptionInfo(
    val throwable: Throwable,
    @StringRes val messageRes: Int
)

