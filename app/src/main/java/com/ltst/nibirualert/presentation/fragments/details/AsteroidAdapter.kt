package com.ltst.nibirualert.presentation.fragments.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ltst.nibirualert.R
import com.ltst.nibirualert.presentation.fragments.details.model.VisitsDetail
import kotlinx.android.synthetic.main.history_visit_list_item.view.*

class AsteroidAdapter : RecyclerView.Adapter<AsteroidHolder>() {

    private val asteroidVisits = ArrayList<VisitsDetail>()

    override fun getItemCount() = asteroidVisits.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AsteroidHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.history_visit_list_item, parent, false)
        return AsteroidHolder(view)
    }

    override fun onBindViewHolder(holder: AsteroidHolder, position: Int) {
        holder.bind(asteroidVisits[position])
    }

    fun swapItems(response: List<VisitsDetail>) {
        asteroidVisits.clear()
        asteroidVisits.addAll(response)
    }
}

class AsteroidHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(visit: VisitsDetail) {
        itemView.visit_date_time.text = visit.approachDate
        itemView.visit_relative_velocity.text = visit.relativeVelocity
        itemView.visit_miss_distance.text = visit.missDistance
        itemView.visit_orbit.text = visit.orbitPlanet
    }
}